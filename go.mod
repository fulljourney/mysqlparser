module gitlab.com/fulljourney/mysqlparser

go 1.15

require (
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/protobuf v1.4.2
	github.com/google/go-cmp v0.5.1
	github.com/stretchr/testify v1.6.1
	golang.org/x/net v0.0.0-20200822124328-c89045814202
	google.golang.org/grpc v1.31.0
	google.golang.org/protobuf v1.23.0
)
