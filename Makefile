# This is only needed for upgrades / merges from the Vitess project.
# The procedure is:
# - make
# - check git for changes
#    - discard overwrites to old fixes and changes
#    - merge new code
# - make sql.go
# - go test ./...
# - commit changes

PROTOC_PATH=./.bin/
PROTOC_BIN:=$(PROTOC_PATH)protoc
PROTOC_BIN_VERSION:=$(lastword $(shell $(PROTOC_BIN) --version))
PROTOS_PATH=./protos/
PKG_PATH=./pkg/
PROTOBUF_PATH=$(PKG_PATH)pb/
PROTOC_CMD=$(PROTOC_BIN) -I$(PROTOS_PATH)

PROTOC_VERSION=3.12.4
# PROTOC_VERSION=3.11.4
# This is the version that is used by the Vitess project according to:
# https://github.com/vitessio/vitess/blob/master/bootstrap.sh#L112
# PROTOC_VERSION=3.6.1

OS=$(shell uname -s)
ARCH=$(shell uname -m)

# This makefile will break on windows and we REALLY don't care
# All of this is for initializing gRPC stuff and doesn't concern building otherwise
ifeq ($(OS),Darwin)
	OS_NAME:=osx
else
	OS_NAME:=$(OS)
endif

.PHONY: all update-from-vitess update-modules update-protoc protos clean

all: install_protoc-gen-go update-protoc update-from-vitess $(PROTOC_BIN) $(GOPATH)/bin/protoc-gen-go protos update-modules clean

update-from-vitess:
	rm -fr vitess-7.0.0* *.go pkg protos sql.y test test_queries visitorgen y.output
	curl -o vitess-7.0.0.zip https://codeload.github.com/vitessio/vitess/zip/v7.0.0
	unzip vitess-7.0.0.zip
	rm vitess-7.0.0.zip

	mkdir -p pkg test protos

	cp ./vitess-7.0.0/go/vt/sqlparser/*.go ./
	rm sql.go
	cp ./vitess-7.0.0/go/vt/sqlparser/sql.y ./
	cp -r ./vitess-7.0.0/go/vt/sqlparser/test_queries ./
	cp -r ./vitess-7.0.0/go/vt/sqlparser/visitorgen ./
	cp -r ./vitess-7.0.0/go/bytes2 ./pkg/
	cp -r ./vitess-7.0.0/go/exit ./pkg/
	cp -r ./vitess-7.0.0/go/hack ./pkg/
	cp -r ./vitess-7.0.0/go/vt/log ./pkg/
	cp -r ./vitess-7.0.0/go/vt/vtgate/evalengine ./pkg/
	cp -r ./vitess-7.0.0/go/sqltypes ./pkg/
	cp -r ./vitess-7.0.0/go/tb ./pkg/
	cp -r ./vitess-7.0.0/go/vt/vterrors ./pkg/
	cp -r ./vitess-7.0.0/go/test/utils ./test/

	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/sqlparser/visitorgen"#"gitlab.com/fulljourney/mysqlparser/visitorgen"#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/proto/#"gitlab.com/fulljourney/mysqlparser/pkg/pb/#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/vtgate/#"gitlab.com/fulljourney/mysqlparser/pkg/#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/vt/#"gitlab.com/fulljourney/mysqlparser/pkg/#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/test/utils"#"gitlab.com/fulljourney/mysqlparser/test/utils"#g'
	find ./ -name "*.go" | xargs sed -i "" -e 's#"vitess.io/vitess/go/#"gitlab.com/fulljourney/mysqlparser/pkg/#g'

	cp ./vitess-7.0.0/proto/query.proto ./protos/
	cp ./vitess-7.0.0/proto/topodata.proto ./protos/
	cp ./vitess-7.0.0/proto/vtrpc.proto ./protos/
	cp ./vitess-7.0.0/proto/vttime.proto ./protos/

	find ./protos -name "*.proto" | xargs sed -i "" -e 's#option go_package = "vitess.io/vitess/go/vt/proto/#option go_package = "gitlab.com/fulljourney/mysqlparser/pkg/pb/#g'
	find ./protos -name "*.proto" | xargs sed -i "" -e 's#option java_package="io.vitess.proto";##g'

install_protoc-gen-go:
	go install github.com/golang/protobuf/protoc-gen-go

# ensure all go modules are installed and at the right version
update-modules:
	go mod tidy

# install protoc if it's missing
$(PROTOC_BIN):
	mkdir -p $(PROTOC_PATH)
	cd $(PROTOC_PATH) && \
		rm -fr * && \
		curl -L https://github.com/protocolbuffers/protobuf/releases/download/v$(PROTOC_VERSION)/protoc-$(PROTOC_VERSION)-$(OS_NAME)-$(ARCH).zip -o protoc.zip && \
		unzip protoc.zip && \
		mv bin/protoc ./ && \
		rm -fr bin && \
		rm protoc.zip
	rm -fr $(PROTOS_PATH)google
	# cp -r $(PROTOC_PATH)include/google $(PROTOS_PATH)

# check if protoc needs to be upgraded
update-protoc: $(PROTOC_BIN)
ifeq ($(PROTOC_BIN_VERSION), )
	# protoc should be automatically installed in the next step
else ifneq ($(PROTOC_BIN_VERSION), $(PROTOC_VERSION))
	rm -fr $(PROTOC_PATH)
	make $(PROTOC_BIN)
endif

# Create protobuf files from .proto files
PROTO_SRCS = $(wildcard protos/*.proto)
PROTO_SRC_NAMES = $(basename $(notdir $(PROTO_SRCS)))
PROTO_GO_OUTS = $(foreach name, $(PROTO_SRC_NAMES), $(PROTOBUF_PATH)$(name)/$(name).pb.go)

# This rule rebuilds all the go files from the proto definitions for gRPC.
protos: $(PROTO_GO_OUTS)

$(PROTO_GO_OUTS): install_protoc-gen-go protos/*.proto
	mkdir -p $(PROTOBUF_PATH)$(basename $(basename $(notdir $@)))
	$(PROTOC_CMD) --go_out=plugins=grpc:$(PROTOBUF_PATH)$(basename $(basename $(notdir $@))) --go_opt=paths=source_relative protos/$(basename $(basename $(notdir $@))).proto
	goimports -w $@

sql.go: sql.y
	go run golang.org/x/tools/cmd/goyacc -o sql.go sql.y
	gofmt -w sql.go
	rm -f y.output

clean:
	rm -fr vitess-7.0.0*
	rm -f y.output
	rm -fr mysqlparser-master
